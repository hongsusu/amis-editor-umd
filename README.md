# amis-editor-umd

## 项目
```
amis 是百度开源的一个低代码前端框架,通过JSON配置就能实现后台管理所需要的所有页面,非常好用.   
在项目中有时候都懒得用vue了,所以可以直接通过sdk的形式引用.    
amis 有一个页面设计器 amis-editor,不过没有开源, react又不属性 ,琢磨了好久,捣鼓出一个基于vue的方案,并且可以直接在script引用.
```

## 更新
### 2021年12月04日
1 更新 amis-editor 版本 4.0.1-beta.10  
2 优化打包体积  
3 整理演示文件,默认引用 .min.js 加速查看  


## 安装
```
yarn install
```

### 运行
```
yarn serve
```

### 编译
```
yarn build
```

### 发布LIB
```
yarn lib
```

